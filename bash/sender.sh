#!/bin/bash
cut -f1 source.tsv > IDAT.sender.tsv
cut --complement -f1 source.tsv > MDAT.sender.tsv
# Optionally do some first level pseudonymization, if intermediate must not see IDAT
openssl smime -encrypt -in IDAT.sender.tsv -aes256 -out IDAT.sender.tsv.smime intermediate.crt
openssl smime -encrypt -in MDAT.sender.tsv -aes256 -out MDAT.sender.tsv.smime receiver.crt

