#!/bin/bash
openssl smime -decrypt -in IDAT.sender.tsv.smime -out IDAT.intermediate.tsv -inkey intermediate.key
# Pseudomize here
cp IDAT.intermediate.tsv PSN.intermediate.tsv
openssl smime -encrypt -in PSN.intermediate.tsv -aes256 -out PSN.intermediate.tsv.smime receiver.crt
